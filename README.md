# Prophet Explorer Shiny App

**Prophet Explorer** is a [Shiny App](https://shiny.rstudio.com/) that offers an interactive interface to explore the main functions of the [**prophet algorithm**](https://cran.r-project.org/package=prophet), an open source package released by Facebook's Core Data Science team for time series analysis.

The Prophet algorithm is a great tool for quickly generating business forecasts based on time series that have some of the following characteristics:

- hourly, daily, or weekly observations with at least a few months (preferably a year) of history
- strong multiple "human-scale" seasonalities: day of week and time of year
- important holidays that occur at irregular intervals that are known in advance (e.g. the Super Bowl)
- a reasonable number of missing observations or large outliers
- historical trend changes, for instance due to product launches or logging changes
- linear trends or logistic trends that saturate at natural limits

To launch the app, clone the repo and run `code/App.R` in rstudio.




